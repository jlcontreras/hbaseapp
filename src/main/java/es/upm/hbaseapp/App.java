package es.upm.hbaseapp;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;


        
public class App {
	
	private static final String groupID = "07";
	
	// Table, column families and column names
	public static final byte[] TABLE = Bytes.toBytes("twitterStats");
    public static final byte[] CF_LANG = Bytes.toBytes("Language");
    public static final byte[] COL_LANG = Bytes.toBytes("Language");
    public static final byte[] CF_DATA = Bytes.toBytes("Data");
    public static final String COL_HASHTAG_PREFIX = "Hashtag";
    
	public static void main( String[] args ) throws MasterNotRunningException, ZooKeeperConnectionException, IOException
    {

        String mode = args[0];
        String zkHost = args[1];
        byte[] startTs = Bytes.toBytes(args[2]);
	    byte[] endTs   = Bytes.toBytes(args[3]);
	    String startTs_str = args[2];
	    String endTs_str = args[3];
        String N = args[4];
        String lang = args[5];
        String dataFolder = args[6];
        String outputFolder = args[7];
        
		Configuration conf = HBaseConfiguration.create();
		HConnection conn = HConnectionManager.createConnection(conf);
	    conf.set("hbase.zookeeper.quorum", zkHost.split(":")[0]);
	    conf.set("hbase.zookeeper.property.clientPort", zkHost.split(":")[1]);
    	HTable table = new HTable(TableName.valueOf(TABLE),conn);
    	

//	    String mode = "4";
//	    String zkHost = null;
//	    String startTs_str = "1448471400000";
//	    String endTs_str = "1448472600000";
//	    byte[] startTs = Bytes.toBytes(startTs_str);
//	    byte[] endTs   = Bytes.toBytes(endTs_str);
//	    String N = "3";
//	    String lang = "fr,en";
//	    String dataFolder = "/home/jose/Documents/simpledataProject2/";
//	    ///Users/Cadarso/Downloads/simpledataProject2
//	    String outputFolder = "/home/jose/Documents/simpledataProject2/";

        String[] result = null;

	    switch(mode){
	    	case "1": 
	    		result = query1(startTs, endTs, N, lang, table,false);
	    		break;
	    	case "2":
	    		result = query2(startTs, endTs, N, lang,table);
	    		break;
	    	case "3":
	    		result = query3(startTs, endTs, N, table);
	    		break;
	    	case "4":
	    		query4(dataFolder, lang, conf, table);
	    		break;
	    	default:
	    		System.out.println("Input a mode between 1 and 4");
	    		break;
	    }
	    
	    // Write results to text file
	    if(!mode.equals("4")){
	    	String file = outputFolder + groupID + "query" + mode + ".out";
	    	for(String line: result){
	    		System.out.println(line);
	    		if(line.split(":").length > 1){
	    			String outLang = line.split(":")[0];
		    		int i = 0;
		    		for(String htag: line.split(":")[1].split(",")){
		    			i++;
		    			String output = outLang + "," + 
		    							i + "," + htag + "," +
		    							startTs_str + "," + endTs_str;
		    			writeResults(file, output);
		    		}	
	    		}
	    	} 	
	    }
	       	
    }
	
    /**
     * Query 1: get top hashtags for a given language and timespan
     */
	public static String[] query1(byte[] startTs, byte[] endTs, String N, String lang, HTable table,boolean query3) 
			throws IOException {	
		Scan scan = new Scan(startTs, endTs);
		if(!query3){
		Filter f =new SingleColumnValueFilter(CF_LANG,COL_LANG,CompareOp.EQUAL,Bytes.toBytes(lang));
		scan.setFilter(f);
		}
		ResultScanner rs = table.getScanner(scan);
		Result res;
		TreeMap<String, Integer> htCount = new TreeMap<String, Integer>();
        while ((res = rs.next()) != null && !res.isEmpty()) {
			Get get = new Get(res.getRow());
			get.setMaxVersions(1);
			Result result = table.get(get);
			while (result != null && !result.isEmpty() && result.advance() ) {
				Cell current = result.current();
                // Get column family, as we are only interested in results from the Data column family
				String fam = Bytes.toString(current.getFamilyArray(), current.getFamilyOffset(),
						current.getFamilyLength());
				if(fam.equals(Bytes.toString(CF_DATA))){
    				String val = Bytes.toString(current.getValueArray(), current.getValueOffset(),
    						current.getValueLength());
    				htCount = updateMap(htCount, val);
                }
		    }
		}
        
		String top = getTopNEntries(htCount, Integer.parseInt(N));  
		String[] topList = new String[1];
		if(!query3){
		topList[0] = lang + ":" + top;
		}
		else topList[0] = "All:" + top;
		return topList;
	}

    /**
     * Query 2: Get top hastags for each of the given languages, in a certaim timespan
     */
	public static String[] query2(byte[] startTs, byte[] endTs, String N, String langList, HTable table) throws IOException{
		int i=0;
		String[] topList = new String[langList.split(",").length];
		for (String lang : langList.split(",")) {	
		topList[i]=query1(startTs, endTs, N, lang, table,false)[0]; 
		i++;
		}
		return topList;
	}

    /**
     * Query 3: Get top hashtags for a timespan, no language restrictions
     *
     *
     */
	public static String[] query3(byte[] startTs, byte[] endTs, String N, HTable table)
			throws IOException {
		return query1(startTs, endTs, N, null, table,true); 
	}

    /**
     * Query 4: Initialize the table and load the data
     */
	public static void query4(String dataFolder, String langlist, Configuration conf, HTable table) 
			throws MasterNotRunningException, ZooKeeperConnectionException, IOException{
        // Create the table
        HBaseAdmin admin = new HBaseAdmin(conf);
        
        // Only create the table if it does not exist
        if(!admin.tableExists(table.getName())){
        	// Define the table: name, column families
            HTableDescriptor table_desc = new HTableDescriptor(TableName.valueOf(TABLE));
            HColumnDescriptor family_lang = new HColumnDescriptor(CF_LANG);
            HColumnDescriptor family_data = new HColumnDescriptor(CF_DATA);
            
            family_lang.setMaxVersions(1); // Default is 3.
            family_data.setMaxVersions(1);
            table_desc.addFamily(family_lang);
            table_desc.addFamily(family_data);
            admin.createTable(table_desc);      
            admin.close();
        }
        
        // Load the data from the input file
		DataLoader dl = new DataLoader();
		dl.loadData(dataFolder, langlist, table);

		table.close();
	}

	/**
	 * Updates the map passed as parameter to include the new value
	 * @param map
	 * @param newVals
	 * @return
	 */
	private static TreeMap<String,Integer> updateMap(TreeMap<String, Integer> map, String newVals){
		String htag = newVals.split(":")[0];
		int count = Integer.parseInt(newVals.split(":")[1]);	
		// Check if the hashtag is already in the map, if so, sum 
		if (map.containsKey(htag)) {
            map.put(htag, map.get(htag) + count);
        } else {
            map.put(htag, count);
        }
		return map;
	}
	
	
	/**
	 * Returns a string representing the top N entries of the input map
	 * @param map TreeMap<String, Integer> 
	 * @param n number of results to retrieve
	 * @return
	 */
	private static String getTopNEntries(TreeMap<String, Integer> map, int n){
		String topEntries = "";
		// No need to worry about alphabetical order because TreeMap
		// is ordered alphabetically.
		// Look for the top n hashtags and put them in the return string
		for (int i = 0; i < n; i++) {
			Entry<String, Integer> maxEntry = null;
			for (Entry<String, Integer> entry : map.entrySet()) {
				if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
					maxEntry = entry;
				}
			}
			if (null != maxEntry) {
				topEntries = topEntries + "," + maxEntry.getKey();
				map.remove(maxEntry.getKey());
			}
		}
		// Removing the initial comma
		if(!topEntries.isEmpty())
			topEntries = topEntries.substring(1);
		
		return topEntries;
	}
	
	/**
	 * Conveniency method for writting output to a file
	 */
	private static void writeResults(String file, String s) {
		try {
			FileWriter fw = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			out.println(s);
			out.close();
			fw.close();
		} catch (IOException e) {
			// TODO: error
		}
	}
	

}
