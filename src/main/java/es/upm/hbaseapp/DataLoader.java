package es.upm.hbaseapp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

public class DataLoader {
	
	/**
	 * Only for testing
	 */
	public static void main(String[] args) throws IOException{
		String testFile = "/home/jose/Documents/simpledataProject2/";
		String testLangs = "en,fr";
		
		byte[] TABLE = Bytes.toBytes("twitterStats");
		Configuration conf = HBaseConfiguration.create();
		HConnection conn = HConnectionManager.createConnection(conf);
    	HTable table1 = new HTable(TableName.valueOf(TABLE),conn);
    
		DataLoader dl = new DataLoader();
		dl.loadData(testFile, testLangs, table1);

		table1.close();
	}
	
	/**
	 * Method to load data from a csv into HBase
	 * @param datafolder folder containing the files to load data from. 
	 * Each line has the format time,lang,htag1,count1,htag2,count2,htag3,count3
	 * @param table HTable where data is stored
	 */
	public void loadData(String datafolder, String langlist, HTable table) {
        String line = "";
        String csvSplitBy = ",";
        
        // For each language in the input list, open the corresponding file and load the data
        for(String lang: langlist.split(",")){
        	
        	String csvFile = datafolder + lang + ".out";
        	
        	try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

                while ((line = br.readLine()) != null) {
                    // Split using use comma as separator
                    String[] linedata = line.split(csvSplitBy);

                    String timestamp = linedata[0];
                    // Create the put operation and add all needed values
                    Put put = new Put(Bytes.toBytes(timestamp+lang));
    		        put.add(App.CF_LANG, App.COL_LANG, Bytes.toBytes(lang));

                    for(int i = 1; i <= 3; i++){
                    	String val = linedata[2*i] + ":" + linedata[2*i+1];
                    	put.add(App.CF_DATA, Bytes.toBytes(App.COL_HASHTAG_PREFIX + i), Bytes.toBytes(val));
                    }    		        
    		        table.put(put);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }   
    }
	

}
